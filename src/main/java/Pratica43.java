/**
 *
 * @author nmessias
 */

import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

public class Pratica43 {
   
    public static void main(String[] args) {
        Retangulo r = new Retangulo(4, 2);
        Quadrado q = new Quadrado(4);
        TrianguloEquilatero t = new TrianguloEquilatero(4);
        
        System.out.println("Retangulo: área = " + r.getArea() + ", perimetro = " + r.getPerimetro());
        System.out.println("Quadrado: área = " + q.getArea() + ", perimetro = " + q.getPerimetro());
        System.out.println("Triangulo equilatero: área = " + t.getArea() + ", perimetro = " + t.getPerimetro());
    }
}
