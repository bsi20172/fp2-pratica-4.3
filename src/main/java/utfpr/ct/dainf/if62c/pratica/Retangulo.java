/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.Serializable;

/**
 *
 * @author nmessias
 */
public class Retangulo implements FiguraComLados, Serializable{
    
    private double base;
    private double altura;
    
    public Retangulo(double base, double altura){
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double getLadoMenor() {
        return this.base;
    }

    @Override
    public double getLadoMaior() {
        return this.altura;
    }

    @Override
    public String getNome() {
        return this.getClass().getSimpleName();
    }

    @Override
    public double getPerimetro() {
        return (base + altura) * 2; 
    }

    @Override
    public double getArea() {
        return base * altura;
    }
    
}
