/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.Serializable;

/**
 *
 * @author nmessias
 */
public class Circulo extends Elipse implements Figura, Serializable{
    
    public Circulo(double raio){
        super(raio, raio);
    }
    
    @Override
    public double getPerimetro(){
        return 2 * Math.PI * super.getEixoMenor();
    }
}
